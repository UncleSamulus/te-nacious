#!/bin/bash
: ' Bulk Data Download Script

'

downextract() {
    local ressource
    ressource="$1"
    ressource_file_name="${ressource##*/}"
    ressource_file_name=$(echo "$ressource_file_name" | sed 's/\.gz$//')
    if [[ -e $ressource_file_name ]]; then
        echo "File $ressource_file_name already exists, skipping download"
        return
    fi
    wget -nc "$ressource"
    gunzip -k "${ressource##*/}"
}

# Reference Proteome
downextract "ftp://ftp.flybase.net/releases/FB2021_01/dmel_r6.38/fasta/dmel-all-translation-r6.38.fasta.gz"

# Transposons
downextract "ftp://ftp.flybase.net/releases/FB2021_01/dmel_r6.38/fasta/dmel-all-transposon-r6.38.fasta.gz"

# Download family to superfamily mapping
wget -nc https://raw.githubusercontent.com/bardin-lab/dmel-transposon-reference-data/master/annotation_files/family_to_superfamily.tab