# Data Source

- `dmel-all-translation-r6.38.fasta`
- `dmel-all-transposon-r6.38.fasta` 
- `Dmelanogaster_6_38_positions_genes.list` - *Drosophila melanogaser* Gene positions
- `family_to_superfamily.tab` - *D. melanogaster* transposon family name to superfamily map: <https://raw.githubusercontent.com/bardin-lab/dmel-transposon-reference-data/master/annotation_files/family_to_superfamily.tab>

## Références

- <https://ftp.flybase.net/releases/FB2021_01/dmel_r6.38/fasta/>
- <https://github.com/bardin-lab/dmel-transposon-reference-data>