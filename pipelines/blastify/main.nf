#!/usr/bin/env nextflow

/** Perform BLAST analyzis of Transposable Elements copy against family reference
 *
 */

nextflow.enable.dsl=2

project_dir = projectDir

params.reference_file_mapping = './data/references/liste_familles_ET_fichierref.txt'
params.data_references_dir = './data/references'
params.data_work_dir = './data/work/align'
params.blast_db_dir = './data/work/align/db'

/*
 * Convert reference file to fasta
 */

process convert_reference {
    input:
        tuple val(family), path(reference_file)
    output:
        file("${family}.fasta")
    script:
        """
        seqret "${reference_file}" "${params.data_work_dir}/${family}.fasta" -osformat2 fasta
        """
}

/* 
 * Extract TE families and reference files and map to blastify process
 */
workflow {
    Channel.fromPath(params.reference_file_mapping) \
        | splitCsv(header: true, sep: ' ') \
        | map { row -> tuple(row.Nom_ET, file("${params.data_references_dir}/${row.Nom_fichier_ref}")) } \
        | convert_reference
}