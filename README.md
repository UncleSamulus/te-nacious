# Bioinformatics Project Report - TE-nacious Flies

Analysis of Transposable Elements (TEs) in the fruitfly *Drosophila melanogaster*.

<div width="500vw" height="auto" style="text-align:center">
<img src="./misc/logo.svg" alt="TE-nacious Flies study logo" />
<p style="text-align: right">drosophila-redeyes icon by Servier https://smart.servier.com/ is licensed under CC-BY 3.0 Unported https://creativecommons.org/licenses/by/3.0/</p>
</div>

---

A L3 student-project at *Licence Génomique Biologie Informatique* (GBI) and *Licence Double Sciences de la Vie - Informatique* (SDVI) at Évry - Paris-Saclay University.

## Authors

- Curie Jordane Kakpovi (GBI)
- Samuel Ortion (SDVI)

## Supervisor

- Carène RIZZON

## License

This project is licensed under the terms of the [MIT license](LICENSE).