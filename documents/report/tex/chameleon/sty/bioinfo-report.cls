%% bioinfo-report.cls
%% Copyright 2023 Samuel ORTION <samuel+dev@ortion.fr>
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Samuel ORTION <samuel+dev@ortion.fr>.
%
% This work consists of the files bioinfo-report.cls and is part of the Chameleon Press project

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Chameleon Bioinformatics Report LuaLaTeX Class
% 
% Author: Samuel ORTION
% Version: v0.0.1
% Created on: 2023-01-06
% Updated on: 2023-01-06
% License: LPPL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLASS OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{chameleon/sty/bioinfo-report}[2023-01-06 v0.0.1 Chameleon Bioinformatics Report LuaLaTeX Class]

\LoadClass{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MISC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{hyperref}
\RequirePackage{etoolbox}
\RequirePackage{calc}
\RequirePackage{luatextra}
\RequirePackage{pgffor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TEXT & FONTS OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{fontspec}
\setmainfont{Linux Libertine O}

\RequirePackage{ulem}
\RequirePackage{lettrine}
\RequirePackage{microtype}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLOR OPRTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{xcolor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PAGE OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{geometry}
\geometry{a4paper, margin=2cm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SECTION OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\RequirePackage[explicit]{titlesec}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLOAT OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{graphicx}
\RequirePackage{caption}
\RequirePackage{subcaption}
\RequirePackage{float}
\RequirePackage{wrapfig}

\usepackage{url}
\usepackage{array}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{lipsum}
\usepackage{sectsty}
\usepackage{tikz}

% 
\RequirePackage{csquotes}
\RequirePackage{polyglossia}
\setmainlanguage{french}
\setotherlanguage{english}

\RequirePackage{mathtools}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CUSTOM COMMANDS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{chameleon/sty/bioinfo-remarks}

%-------------------------------------------------------------------------
% FLOAT LABELS
%-------------------------------------------------------------------------
\definecolor{UoDBlue}{RGB}{67, 101, 226}
\definecolor{UoDDarkBlue}{RGB}{61, 88, 151}
\definecolor{UoDLightBlue}{RGB}{209,226,242}

\colorlet{colorprimary}{UoDBlue}


\partfont{\color{colorprimary}}
\sectionfont{\color{colorprimary}}
\subsectionfont{\color{colorprimary}}
\subsubsectionfont{\color{colorprimary}}


% Define a font for figure labels
\captionsetup{
    figurename=Figure,
    tablename=Tableau,
    labelfont={bf,color=colorprimary},
}

% Align right captions
\captionsetup{justification   = raggedright,
              singlelinecheck = false}

% Declare a command for figure source, in figure environment
\newcommand{\source}[1]{\vspace{-3pt} \caption*{ Source: {#1}} }

\RequirePackage{polyglossia}
\setmainlanguage{french}
\addto\captionsfrench{\renewcommand*{\partname}{Partie}}

\RequirePackage{titlesec}


% \titleclass{\part}{top}
% \titleformat{\part}[display]
%   {\huge\bfseries\centering\color{colorprimary}}{Partie~\thepart}{0pt}{}
% \titlespacing*{\part}{0pt}{40pt}{40pt}

% % Set section formatting to "A." (Alph)
% \titleformat{\section}
%   {\normalfont\large\bfseries\color{colorprimary}}{\Alph{section}.}{1em}{}

\RequirePackage{minted}
\setminted{
    % bgcolor=mintedbackground,
    fontfamily=tt,
    linenos=true,
    numberblanklines=true,
    numbersep=12pt,
    numbersep=5pt,
    gobble=0,
    frame=leftline,
    framesep=2mm,
    funcnamehighlighting=true,
    tabsize=4,
    obeytabs=false,
    mathescape=false
    samepage=false,
    showspaces=false,
    showtabs =false,
    texcl=false,
    baselinestretch=1.2,
    breaklines=true,
}
\usemintedstyle{vs}

\RequirePackage{booktabs}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black},
    backref=page
}

% \hfuzz=11pt 

\usepackage[nameinlink]{cleveref}