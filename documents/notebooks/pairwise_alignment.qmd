---
title: Pairwise alignment of transposons
author: Samuel Ortion
date: 2023-03-30
---

We will perform blast analyzis of transposons copies from each families, in order to see how they diverged from the reference sequence.

## Source files description

## Blast analysis


1. Convert reference EMBL Sequences to FASTA format

Example for `gypsy12`:

```bash
seqret  ./data/references/gypsy12.embl ./data/out/align/gypsy12.fasta 
```

2. Extract all `gypsy12` transposons hits from `./data/dmel-all-transposon-r6.38.fasta`

```awk
#!/bin/awk -f
# Extract transposon sequences from a FASTA file containing multiple transposon sequences
# Usage: ./extract_family_sequences.awk -v target="gypsy12" input.fasta > output.fasta
# where target is the family name to extract
# Detect the beginning of a new sequence of the target family
/^>/ {
    name = $4
	gsub("name=", "", name)
	split(name, name_array, "{}")
	gsub(" ", "", name_array[1])
	family = name_array[1]
	if (family ~ target) {
        print $0
        flag = 1
    } else {
        flag = 0
    }
}

# Print nucleotide sequence only if it is a part of a hit of the target family
/^[^>]/ && flag == 1 {
    print $0
}
```

```bash
./src/extract_family_sequences.awk -v target="gypsy12" ./data/dmel-all-transposon-r6.38.fasta > ./data/out/align/gypsy12-all.fasta
```

3. Perform blast analysis

```bash
makeblastdb -in ./data/out/align/gypsy12-all.fasta -parse_seqids -dbtype nucl -out data/out/align/db/gypsy12
```

```bash
blastn -query ./data/out/align/gypsy12.fasta -db ./data/out/align/db/gypsy12 -outfmt 7 -out ./data/out/align/gypsy12.blast
```

```text
GYPSY12	FBti0060275	99.550	10001	8	3	255	10218	1	10001	0.0	18185
GYPSY12	FBti0060275	100.000	2336	0	0	1	2336	7666	10001	0.0	4314
GYPSY12	FBti0060275	97.876	2119	8	3	8137	10218	1	2119	0.0	3629
GYPSY12	FBti0060275	95.522	67	2	1	2932	2998	2659	2724	6.62e-24	106
GYPSY12	FBti0060275	95.522	67	2	1	2876	2941	2715	2781	6.62e-24	106
GYPSY12	FBti0060690	82.332	832	120	9	5820	6639	1	817	0.0	697
GYPSY12	FBti0060690	77.990	577	91	23	828	1386	10637	11195	3.61e-91	329
GYPSY12	FBti0060690	77.990	577	91	23	8710	9268	10637	11195	3.61e-91	329
GYPSY12	FBti0063052	78.017	1160	194	26	5046	6193	916	2026	0.0	673
GYPSY12	FBti0061766	86.200	471	57	7	1798	2265	1513	1978	2.01e-143	503
```
This blast report in TSV format contains the following columns:

```text
qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
```

|Column|Description|Comment|
|---|---|---|
|`qseqid`|Query (e.g., gene) sequence id|Here the reference transposon|
|`sseqid`|Subject (e.g., reference genome) sequence id|Here the transposon hit id|
| `pident` | Identity percentage | |
| `length` | Alignment length | |
| `mismatch` | Number of mismatches | |
| `gapopen` | Number of gap openings | |
| `qstart` | Start of alignment in query | |
| `qend` | End of alignment in query | |
| `sstart` | Start of alignment in subject | |
| `send` | End of alignment in subject | |
| `evalue` | $e$-value |
| `bitscore` | Score |


A bash script is written in order to automate the process detailed above.

We can then use:
```bash
 ./src/transposon_blast_ref2hit.sh <family_name>
```
for each family of interest, and it will generate the fasta sequence subset, reference fasta, and blast report.


```bash
for family in hopper HMS-Beagle BS; do
  ./src/transposon_blast_ref2hit.sh $family
done
```

## Sanity check

```bash
for family in hopper HMS-Beagle BS; do
  echo "Checking $family family"
  hsp_count=$(grep -c "^[^>]" "./data/out/align/$family.blast")
  if [ $hsp_count -eq 0 ]; then
    echo "No HSP found in $family family"
  else
    echo "$hsp_count HSPs found in $family family"
  fi
done
```

```text
Checking hopper family
35 HSPs found in hopper family
Checking HMS-Beagle family
72 HSPs found in HMS-Beagle family
Checking BS family
73 HSPs found in BS family
```

As wanted, we find several HSPs for each family of interest.