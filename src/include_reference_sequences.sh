#!/usr/bin/env bash
: '
Create a TSV table with reference sequence entries into `transposable_elements` table.
'

family_names() {
    awk '
    NR > 1 {
        print $1
    }
    ' ./data/references/liste_familles_ET_fichierref.txt
}

blastify() {
    local family_name
    family_name="$1"
    ./src/transposon_blast_ref2hit.sh "$family_name"
}

sequence_length() {
    local file_name
    file_name="$1"
    ./src/fasta_sequence_length.awk -v idx=1 $file_name
}

sequence_id() {
    local fasta
    fasta="$1"
    awk '
    NR == 1 && /^>/ {
        sequence_id=$1
        gsub(/^>/, "", sequence_id)
        print sequence_id
    }' $fasta
}

gimmedata() {
    local family_name
    family_name="$1"
    # $(blastify "$family_name")
    local file_name
    file_name="./data/out/align/$family_name.fasta"
    if [[ -e $file_name ]]; then
        length=$(sequence_length "$file_name")
        sequence_id=$(sequence_id "./data/out/align/$family_name.fasta")
        # Dirty fix for erroneous sequence ids in fasta files
        if [[ $family_name = "NOF" ]]; then
            sequence_id="X15469"
        fi
        if [[ $family_name = "FB" ]]; then
            sequence_id="X15469"
        fi
        echo -e $sequence_id'\t'$family_name'\t'$length'\t''true'
    else
        echo "No file $file_name" > /dev/stderr
    fi
}

for family_name in $(family_names); do
    gimmedata "$family_name"    
done

