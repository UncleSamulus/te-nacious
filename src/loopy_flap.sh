#!/usr/bin/env bash

list_match_copies() {
    local blast_file
    blast_file="$1"
    awk -F '\t' '{ print $2 }' "$blast_file" | uniq
}


FAMILY_NAME="$1"
BLAST_DIR="./data/out/align"
REFERENCE_FILE="${BLAST_DIR}/${FAMILY_NAME}.fasta"
BLAST_FILE="${BLAST_DIR}/${FAMILY_NAME}.blast"
REFERENCE_LENGTH="$(./src/fasta_sequence_length.awk -v idx=1 ${REFERENCE_FILE})"
echo "Reference length: ${REFERENCE_LENGTH}"

for copy in $(list_match_copies "$BLAST_FILE"); do
    COPIES_FILE="${BLAST_DIR}/${FAMILY_NAME}-all.fasta"
    COPY_LENGTH=$(./src/fasta_sequence_length.awk -v target="${copy}" "${COPIES_FILE}")
    echo "Copy length: ${COPY_LENGTH}"
    PLOT_TITLE="${FAMILY_NAME} transposon family reference alignment against ${copy} copy"
    echo "Generating FLAP plot for ${copy}"
    python3 ./tools/flap/flap.py --blast ${BLAST_FILE} --title "${PLOT_TITLE}" --query-name "ref" --subject-name "${copy}" --query-length "${REFERENCE_LENGTH}" --subject-length "${COPY_LENGTH}" --center --color-map rainbow -g --min-identity 70
done
