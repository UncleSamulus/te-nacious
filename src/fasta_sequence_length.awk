#!/usr/bin/env -S awk -f
# Extract sequence length from FASTA file and sequence name or sequence position
# Usage: ./fasta_sequence_length.awk -v target=<sequence_id> <sequences.fasta>
# where <sequence_id> is the name of the sequence to retrieve the length for
# and <sequences.fasta> is a file containing several fasta sequence, including the targeted one
# or:
# ./src/fasta_sequence_length.awk -v idx=1 <sequences.fasta> 
# where idx is the position of target sequence in fasta file
# Example:
# ./src/fasta_sequence_length.awk -v target=FBti0019522 ./data/out/align/flea-all.fasta

BEGIN {
    sequence_length=0
    sequence_index=0
}

# Detect target sequence beggining
/^>/ {
    sequence_index++
    seqid=$1
	gsub(">", "", seqid)
	if ((idx == sequence_index) || (target != "" && target ~ seqid)) {
        if (sequence_length > 0) {
            print "Several sequences with same id"
            exit
        }
        flag = 1
    } else {
        flag = 0
    }
}

# Count sequence fragment length if it corresponds to the target sequence 
/^[^>]/ && flag == 1 {
    sequence_length += length($0)
}

END {
    print sequence_length
}