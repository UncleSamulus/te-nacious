#!/bin/awk -f
BEGIN {
    OFS="\t"
    print "id", "family", "chromosome", "start", "end", "length", "complement"
}
/^>/ {
    transposon_id=$1
    gsub(">", "", transposon_id)
    transposon_position=$3
    gsub("loc=", "", transposon_position)
    gsub(";", "", transposon_position)
    split(transposon_position, location_array, ":")
    transposon_chromosome=location_array[1]
    coordinates=location_array[2]
    if (coordinates ~ "complement") {
        complement="+"
        gsub("complement", "", coordinates)
        gsub("\\(", "", coordinates)
        gsub("\\)", "", coordinates)
    } else {
        complement="-"
    }
    split(coordinates, position_array, "\\.\\.")
    transposon_start=position_array[1]
    transposon_end=position_array[2]
    transposon_name=$4
	gsub("name=", "", transposon_name)
	split(transposon_name, name_array, "{}")
	gsub(" ", "", name_array[1])
	transposon_family = name_array[1]
    transposon_length=$7
    gsub(";", "", transposon_length)
    gsub("length=", "", transposon_length)
    print transposon_id, transposon_family, transposon_chromosome, transposon_start, transposon_end, transposon_length, complement
}